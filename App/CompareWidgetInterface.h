#ifndef COMPAREWIDGETINTERFACE_H
#define COMPAREWIDGETINTERFACE_H

#include <QColor>
#include <QtCharts>

class TelemetryData;
class CustomTheme;

class CompareWidgetInterface
{
  public:
	virtual ~CompareWidgetInterface() = default;

	virtual QString name() const = 0;
	virtual QWidget *widget() = 0;

	virtual void home() = 0;

	virtual void addTelemetryData(const QVector<TelemetryData *> &telemetry) = 0;
	virtual void removeTelemetryData(int index) = 0;
	virtual void clear() = 0;

	virtual void setTelemetryVisibility(const QVector<bool> &visibility) = 0;
	virtual void setColors(const QList<QColor> &colors) = 0;

	virtual void setTheme(QtCharts::QChart::ChartTheme theme) = 0;
	virtual void setCustomTheme(const CustomTheme &theme) = 0;

	virtual void setCursor(double value) = 0;
	virtual void setCursorColor(const QColor &color) = 0;
	virtual void setCursorVisible(bool value) = 0;

	virtual void setTelemetryDataDiff(int index, bool diff) = 0;
	virtual void setReference(const TelemetryData *data) = 0;

	virtual void setTrackIndex(int trackIndex) = 0;

	virtual int leftMargin() = 0;
	virtual void setLeftMargin(int value) = 0;
	virtual int strecthFactor() = 0;

	virtual void highlight(int index) = 0;
};

#endif // COMPAREWIDGETINTERFACE_H
