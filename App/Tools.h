#ifndef TOOLS_H
#define TOOLS_H

#include <QImage>
#include <QString>


class Tools
{
  public:
	static QImage loadImageWithColor(const QString &file, const QColor &color);
	static QIcon loadIconWithColor(const QString &file, const QColor &color);

	static bool compareQVariants(const QVariant &v1, const QVariant &v2);
};

#endif // TOOLS_H
