#ifndef F1TELEMETRYSETTINGS_H
#define F1TELEMETRYSETTINGS_H

#include "ApplicationSettings.h"
#include "CompareTelemetryPage.h"
#include "CustomTheme.h"

#include <QChart>


class F1TelemetrySettings : public ApplicationSettings
{
  public:
	F1TelemetrySettings();
	F1TelemetrySettings(const QString &iniFile);

	static std::shared_ptr<F1TelemetrySettings> defaultSettings();

	void reset() override;

	QtCharts::QChart::ChartTheme theme() const;
	void setTheme(QtCharts::QChart::ChartTheme theme);

	CustomTheme customTheme() const;
	void setCustomTheme(const CustomTheme &theme);

	bool useCustomTheme() const;
	void setUseCustomTheme(bool value);

	int linesWidth() const;
	void setLinesWidth(int value);

	int selectedLinesWidth() const;
	void setSelectedLinesWidth(int value);

	QColor cursorColor() const;
	void setCursorColor(const QColor &color);

	int port() const;
	void setPort(int value);

	QString server() const;
	void setServer(const QString &address);

	QString myTeamName() const;
	void setMyTeamName(const QString &name);

	unsigned int driverMarkButton() const;
	void setDriverButton(unsigned int button);

	QString controllerType() const;
	void setControllerType(const QString &type);

	int nbVariablesPerPage();
	void setNbVariablesPerPage(int value);

	QVector<std::shared_ptr<PageSettings>> pageSettings(const QString &type);
	void setPageSettings(const QString &type, const QVector<std::shared_ptr<PageSettings>> &pageSettings);
};

#endif // F1TELEMETRYSETTINGS_H
