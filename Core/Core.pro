QT -= gui
QT += core network


TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ButtonsManager.cpp \
    DriverTracker.cpp \
    F1Listener.cpp \
    FlashbackManager.cpp \
    Lap.cpp \
    Logger.cpp \
    ModeData.cpp \
    Race.cpp \
    Stint.cpp \
    TelemetryData.cpp \
    TelemetryDefinitions.cpp \
    TrackInfo.cpp \
    Tracker.cpp \
    Tyres.cpp \
    UdpSpecification.cpp

HEADERS += \
    ButtonsManager.h \
    DriverTracker.h \
    F1Listener.h \
    FlashbackManager.h \
    Lap.h \
    Logger.h \
    ModeData.h \
    Race.h \
    Stint.h \
    TelemetryData.h \
    TelemetryDefinitions.h \
    TrackInfo.h \
    Tracker.h \
    Tyres.h \
    UdpSpecification.h

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
