#include "Lap.h"
#include "TrackInfo.h"

#include <QFile>
#include <QtDebug>

Lap::Lap(const QVector<TelemetryInfo> &dataInfo) : TelemetryData(dataInfo) {}

QString Lap::description() const
{
	auto time = QTime(0, 0).addMSecs(int(double(lapTime) * 1000.0)).toString("m:ss.zzz");
	auto team = UdpSpecification::instance()->team(driver.m_teamId);
	auto tyre = UdpSpecification::instance()->visualTyre(visualTyreCompound);
	QString additionalInfo;
	if(isOutLap && isInLap) {
		additionalInfo = " (inout)";
	} else if(isOutLap) {
		additionalInfo = " (out)";
	} else if(isInLap) {
		additionalInfo = " (in)";
	}
	return driver.driverFullName() + " - " + tyre + " - " + time + additionalInfo;
}

void Lap::resetData()
{
	clearData();
	ers.clear();
	fuelMix.clear();
	coasting.clear();
	innerTemperatures.frontLeft.clear();
	innerTemperatures.frontRight.clear();
	innerTemperatures.rearLeft.clear();
	innerTemperatures.rearRight.clear();
	startTyreWear.frontLeft = 0.0;
	startTyreWear.frontRight = 0.0;
	startTyreWear.rearLeft = 0.0;
	startTyreWear.rearRight = 0.0;
	isOutLap = false;
	isInLap = false;
	meanBalance = 0.0;
	calculatedTyreDegradation = 0.0;
	calculatedTotalLostTraction = 0.0;
	flashbackManager.clear();
	_mapsData.clear();
}

QVariant Lap::autoSortData() const { return recordDate; }

void Lap::removeLastData()
{
	TelemetryData::removeLastData();

	if(!_mapsData.isEmpty()) {
		_mapsData.removeLast();
	}
}

void Lap::fixDriverNameForMultiplayer(int driverIndex)
{
	driver.m_name.append("").append(QString::number(driverIndex));
}

Lap *Lap::fromFile(const QString &filename)
{
	auto lap = new Lap;
	lap->load(filename);

	return lap;
}

void Lap::saveData(QDataStream &out) const
{
	out << track << session_type << trackTemp << airTemp << weather << invalid << saveGenericData(driver);
	out << recordDate << averageStartTyreWear << averageEndTyreWear << saveGenericData(setup);
	out << comment << lapTime << sector1Time << sector2Time << sector3Time;

	QByteArray telemetryData;
	QDataStream outTelemetry(&telemetryData, QIODevice::WriteOnly);
	TelemetryData::saveData(outTelemetry);
	out << telemetryData;

	int oldFlashbackCount = 0;
	out << tyreCompound << maxSpeed << maxSpeedErsMode << maxSpeedFuelMix << fuelOnStart << fuelOnEnd << ers << energy
		<< harvestedEnergy << deployedEnergy << innerTemperatures << oldFlashbackCount << trackDistance << startTyreWear
		<< endTyreWear << isInLap << isOutLap << visualTyreCompound << meanBalance << energyBalance
		<< calculatedTyreDegradation << calculatedTotalLostTraction << fuelMix << recordVersion
		<< flashbackManager.counts << engineDamageStart << engineDamageEnd << gearboxDamageStart << gearboxDamageEnd
		<< _mapsData << coasting;
}

void Lap::loadData(QDataStream &in)
{
	QByteArray driverData, setupData;
	in >> track >> session_type >> trackTemp >> airTemp >> weather >> invalid >> driverData >> recordDate >>
		averageStartTyreWear >> averageEndTyreWear >> setupData >> comment >> lapTime >> sector1Time >> sector2Time >>
		sector3Time;

	loadGenericData(driver, driverData);
	loadGenericData(setup, setupData);

	QByteArray telemetryData;
	in >> telemetryData;
	QDataStream inTelemetry(&telemetryData, QIODevice::ReadOnly);
	TelemetryData::loadData(inTelemetry);

	int oldFlashbackCount;
	in >> tyreCompound >> maxSpeed >> maxSpeedErsMode >> maxSpeedFuelMix >> fuelOnStart >> fuelOnEnd >> ers >> energy >>
		harvestedEnergy >> deployedEnergy >> innerTemperatures >> oldFlashbackCount >> trackDistance >> startTyreWear >>
		endTyreWear >> isInLap >> isOutLap >> visualTyreCompound >> meanBalance >> energyBalance >>
		calculatedTyreDegradation >> calculatedTotalLostTraction >> fuelMix >> recordVersion >>
		flashbackManager.counts >> engineDamageStart >> engineDamageEnd >> gearboxDamageStart >> gearboxDamageEnd >>
		_mapsData >> coasting;

	if(oldFlashbackCount > 0) {
		flashbackManager.counts[FlashbackType::Other] = oldFlashbackCount;
	}
}

QVariantMap Lap::exportData() const
{
	auto dataMap = TelemetryData::exportData();

	QVariantMap map;
	map["trackId"] = TRACK_INFO(track)->name;
	map["sessionType"] = UdpSpecification::instance()->session_type(session_type);
	map["trackTemp"] = trackTemp;
	map["airTemp"] = airTemp;
	map["weather"] = weather;
	map["recordDate"] = recordDate;
	map["recordVersion"] = recordVersion;
	map["averageStartTyreWear"] = averageStartTyreWear;
	map["averageEndTyreWear"] = averageEndTyreWear;
	map["lapTime"] = lapTime;
	map["sector1Time"] = sector1Time;
	map["sector2Time"] = sector2Time;
	map["sector3Time"] = sector3Time;
	map["maxSpeed"] = maxSpeed;
	map["tyreCompound"] = tyreCompound;
	map["visualTyreCompound"] = visualTyreCompound;
	map["fuelOnStart"] = fuelOnStart;
	map["fuelOnEnd"] = fuelOnEnd;
	map["energy"] = energy;
	map["energyBalance"] = energyBalance;

	map["isInLap"] = isInLap;
	map["meanBalance"] = meanBalance;
	map["calculatedTyreDegradation"] = calculatedTyreDegradation;
	map["calculatedTotalLostTraction"] = calculatedTotalLostTraction;
	map["engineDamageStart"] = engineDamageStart;
	map["gearboxDamageStart"] = gearboxDamageStart;
	map["engineDamageEnd"] = engineDamageEnd;
	map["gearboxDamageEnd"] = gearboxDamageEnd;

	map["startTyreWear"] = startTyreWear.toMap();
	map["endTyreWear"] = endTyreWear.toMap();

	auto driverMap = QVariantMap();
	driverMap["name"] = driver.m_name;
	driverMap["team"] = UdpSpecification::instance()->team(driver.m_teamId);
	driverMap["ia"] = driver.m_aiControlled;
	map["driver"] = driverMap;

	auto setupMap = QVariantMap();
	setupMap["fuelLoad"] = setup.m_fuelLoad;
	setupMap["ballast"] = setup.m_ballast;
	setupMap["frontRightTyrePressure"] = setup.m_frontRightTyrePressure;
	setupMap["frontLeftTyrePressure"] = setup.m_frontLeftTyrePressure;
	setupMap["rearRightTyrePressure"] = setup.m_rearRightTyrePressure;
	setupMap["rearLeftTyrePressure"] = setup.m_rearLeftTyrePressure;
	setupMap["brakeBias"] = setup.m_brakeBias;
	setupMap["brakePressure"] = setup.m_brakePressure;
	setupMap["rearSuspensionHeight"] = setup.m_rearSuspensionHeight;
	setupMap["frontSuspensionHeight"] = setup.m_frontSuspensionHeight;
	setupMap["rearAntiRollBar"] = setup.m_rearAntiRollBar;
	setupMap["frontAntiRollBar"] = setup.m_frontAntiRollBar;
	setupMap["rearSuspension"] = setup.m_rearSuspension;
	setupMap["frontSuspension"] = setup.m_frontSuspension;
	setupMap["rearToe"] = setup.m_rearToe;
	setupMap["frontToe"] = setup.m_frontToe;
	setupMap["rearCamber"] = setup.m_rearCamber;
	setupMap["frontCamber"] = setup.m_frontCamber;
	setupMap["offThrottle"] = setup.m_offThrottle;
	setupMap["onThrottle"] = setup.m_onThrottle;
	setupMap["rearWing"] = setup.m_rearWing;
	setupMap["frontWing"] = setup.m_frontWing;
	map["setup"] = setupMap;

	dataMap["MetaData"] = map;
	return dataMap;
}

const QVector<MapPoint> &Lap::mapsData() const { return _mapsData; }

void Lap::addMapPoint(const MapPoint &point) { _mapsData << point; }

MapPoint Lap::mapPointAt(float x) const
{
	if(_mapsData.isEmpty()) {
		return MapPoint();
	}

	if(_xValues.contains(x)) {
		auto xIndex = _xValues.indexOf(x);
		return _mapsData.value(xIndex);
	}

	auto upperBoundIt = std::upper_bound(_xValues.begin(), _xValues.end(), x);
	if(upperBoundIt != _xValues.end() && upperBoundIt != _xValues.begin()) {
		auto x1Index = upperBoundIt - _xValues.begin();
		auto x0 = _xValues.value(x1Index - 1);
		auto x1 = _xValues.value(x1Index);
		auto y0 = _mapsData.value(x1Index - 1);
		auto y1 = _mapsData.value(x1Index);
		return interpolate(x, x0, y0, x1, y1);
	} else if(upperBoundIt == _xValues.end()) {
		return _mapsData.last();
	}

	return _mapsData.first();
}

QDataStream &operator>>(QDataStream &in, MapPoint &data)
{
	in >> data.position >> data.t >> data.direction;
	return in;
}

QDataStream &operator<<(QDataStream &out, const MapPoint &data)
{
	out << data.position << data.t << data.direction;
	return out;
}

MapPoint operator+(const MapPoint &p1, const MapPoint &p2)
{
	return MapPoint{p1.position + p2.position, p1.t + p2.t, p1.direction + p2.direction};
}

MapPoint operator-(const MapPoint &p1, const MapPoint &p2)
{
	return MapPoint{p1.position - p2.position, p1.t - p2.t, p1.direction - p2.direction};
}

MapPoint operator*(const MapPoint &p, double value)
{
	return MapPoint{p.position * value, p.t * value, p.direction * value};
}

MapPoint operator/(const MapPoint &p, double value)
{
	return MapPoint{p.position / value, p.t / value, p.direction / value};
}
