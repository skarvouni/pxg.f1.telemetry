#include "Race.h"
#include "TrackInfo.h"

#include <QVariant>

Race::Race(const QVector<TelemetryInfo> &dataInfo) : Lap(dataInfo) {}

QString Race::description() const
{
	auto nbLap = QString::number(nbLaps());
	auto team = UdpSpecification::instance()->team(driver.m_teamId);
	auto trackName = TRACK_INFO(track)->name;
	return driver.driverFullName() + " - " + trackName + " - " + nbLap + "Laps";
}

void Race::resetData()
{
	Lap::resetData();

	pitstops.clear();
	stintsLaps.clear();
	stintsVisualTyre.clear();

	penalties = 0;
	nbSafetyCars = 0;
	nbVirtualSafetyCars = 0;
}

QVariant Race::autoSortData() const { return endPosition; }

int Race::nbLaps() const { return countData(); }

Race *Race::fromFile(const QString &filename)
{
	auto race = new Race;
	race->load(filename);

	return race;
}

void Race::saveData(QDataStream &out) const
{
	QByteArray lapData;
	QDataStream outLap(&lapData, QIODevice::WriteOnly);
	Lap::saveData(outLap);
	out << lapData << penalties << nbSafetyCars << nbVirtualSafetyCars << pitstops << stintsLaps << stintsVisualTyre
		<< startedGridPosition << endPosition << pointScored << raceStatus;
}

void Race::loadData(QDataStream &in)
{
	QByteArray lapData;
	in >> lapData;
	QDataStream inLap(&lapData, QIODevice::ReadOnly);
	Lap::loadData(inLap);

	in >> penalties >> nbSafetyCars >> nbVirtualSafetyCars >> pitstops >> stintsLaps >> stintsVisualTyre >>
		startedGridPosition >> endPosition >> pointScored >> raceStatus;
}

QVariantMap Race::exportData() const
{
	auto lapMap = Lap::exportData();

	QVariantMap map;
	map["penalties"] = penalties;
	map["nbSafetyCars"] = nbSafetyCars;
	map["nbVirtualSafetyCars"] = nbVirtualSafetyCars;
	map["startedGridPosition"] = startedGridPosition;
	map["endPosition"] = endPosition;
	map["pointScored"] = pointScored;
	map["raceStatus"] = raceStatus;

	QVariantList pitstopsValues;
	for(const auto &ps : pitstops)
		pitstopsValues << ps;
	map["pitstops"] = pitstopsValues;

	QVariantList stintsLapsValues;
	for(const auto &sl : stintsLaps)
		stintsLapsValues << sl;
	map["stintsLaps"] = stintsLapsValues;

	QVariantList stintsTyreValues;
	for(const auto &st : stintsVisualTyre)
		stintsTyreValues << st;
	map["stintsTyres"] = stintsTyreValues;

	lapMap["Race"] = map;
	return lapMap;
}
